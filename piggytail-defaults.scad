// NOSTL

include <moslib/libchamfer.scad>;
include <peglib/defaults.scad>;
include <peglib/rail.scad>;


wideness = 2;

$fn = 90;

tube_outer_gap = 1;
cap_back_wall_gap = 0.2;
od = (base_size * wideness) - tube_outer_gap;
id = od - (wall_thickness * 2);
chamfer = (wall_thickness / 2) / 2;


