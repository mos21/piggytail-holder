tallness = 4; // magic:tallness:[2,3,4]
caps = 1; // magic:caps:[1,2]
cutout = false; // magic:cutout:[true,false]

include <piggytail-defaults.scad>;

height = base_size * tallness;

// move edge of tube against center line
translate([0, -od / 2, 0]) {
        closed_chamfered_tube(height, od, id, chamfer = chamfer, cutout = cutout);
}

rotate([0, 0, 180]) {
    rail(height = tallness, chamfer = chamfer, back_wall_gap = cap_back_wall_gap, copies = caps);
}


// fill the gap between the rail and the tube
difference() {
    color("blue") {
        indent = wall_thickness;
        translate([0, -(wall_thickness / 4), 0]) {
            chamfered_box([(base_size * caps) > (od - indent)? (od - indent): base_size * caps, (id / 2) + wall_thickness, height], align = [0, -1, 1], chamfer = chamfer);
        }
    }
    color("red") {
        translate([0, -(od / 2), (height / 2)]) {
            cylinder(h = height + 20, d = od - ((od - id) / 2), center = true);
        }
    }
}
